;;;; tty-interface.lisp - TTY interface for cl-menusystem

(defpackage #:cl-menusystem-tty
  (:use #:common-lisp #:cl-menusystem)
  (:export #:tty-menu
	   #:do-menu-loop-tty
	   #:do-inputable-loop-tty))

(in-package #:cl-menusystem-tty)

(defmethod simple-format-message ((output-device stream) control-string &rest format-args)
  (apply #'format (append (list output-device control-string) format-args)))

;;; Supporting the tty-menu class:

(defclass tty-menu (menu)
  ((inputable-backend :accessor inputable-backend)))

(defclass tty-menu-inputable (integer-valued-inputable)
  ((associated-menu :initarg :associated-menu :accessor associated-menu)))

(defmethod initialize-instance :after ((tty-menu-object tty-menu) &rest whatever)
  (declare (ignore whatever))
  (setf (inputable-backend tty-menu-object) (make-instance 'tty-menu-inputable
                                                           :presentable-description "Please select from the following options:"
                                                           :instance-valid-input-predicate #'(lambda (tmi) (let ((pl (presentables (associated-menu tmi))))
                                                                                                             (if (<= 1 (current-state tmi) (1+ (length pl)))
                                                                                                                 t
                                                                                                               (values nil (format nil "Please select an option between 1 and ~A." (1+ (length pl)))))))
                                                           :associated-menu tty-menu-object)))

(defmethod display-object ((tty-menu-object tty-menu) (output-device stream))
  (format output-device "MENU: ~A~&" (presentable-description tty-menu-object))
  (display-object (inputable-backend tty-menu-object) output-device))

(defmethod display-object-briefly ((tty-menu-object tty-menu) (output-device stream))
  (format output-device "~A~&" (presentable-description tty-menu-object)))

(defmethod request-object ((tty-menu-object tty-menu) (output-device stream) (input-device stream))
  (request-object (inputable-backend tty-menu-object) output-device input-device))

;;; argh, multiply defined macros don't work

(defmacro do-inputable-loop-tty (inputable-object output-device input-device)
  (let ((entry-sym (gensym))
	(visp (gensym))
	(inputable-object-name (gensym))
	(output-device-name (gensym))
	(input-device-name (gensym)))
    `(let ((,inputable-object-name ,inputable-object)
	   (,output-device-name ,output-device)
	   (,input-device-name ,input-device))
       (tagbody
	,entry-sym
	(display-object ,inputable-object-name ,output-device-name)
	(request-object ,inputable-object-name ,output-device-name ,input-device-name)
	(let ((,visp (multiple-value-list (valid-inputable-state-p ,inputable-object-name))))
	  (when (not (car ,visp))
	    (simple-format-message ,output-device-name "Invalid entry: ~A~&" (nth 1 ,visp))
	    (go ,entry-sym))))
	(current-state ,inputable-object))))

(defmacro do-menu-loop-tty (menu-object output-device input-device)
  `(do-inputable-loop-tty (inputable-backend ,menu-object) ,output-device ,input-device))

(defmethod display-object ((tty-menu-inputable-object tty-menu-inputable) (output-device stream))
  (format output-device "Please select from the following ~A options:~&" (1+ (length (presentables (associated-menu tty-menu-inputable-object)))))
  (let ((current-index 1))
    (mapcar #'(lambda (pres)
                (format output-device "~A: " current-index)
                (display-object-briefly pres output-device)
		(format output-device "~&")
                (incf current-index)) (presentables (associated-menu tty-menu-inputable-object)))
    (format output-device "~A: Detailed help on this menu~&" current-index)))

(defmethod display-object-briefly ((tty-menu-inputable-object tty-menu-inputable) (output-device stream))
;;  (format output-device "MENU: ~A~&" (presentable-description tty-menu-inputable-object))
  (format output-device "~A option~:P hidden" (1+ (length (presentables (associated-menu tty-menu-inputable-object))))))

(defmethod request-object ((tty-menu-inputable-object tty-menu-inputable) (output-device stream) (input-device stream))
  (format output-device "Select an option from the menu> ")
  (finish-output output-device)
  (setf (current-state tty-menu-inputable-object) (parse-integer (read-line input-device) :junk-allowed t)))

;;; This code contains display support for the other types of presentables

;;; A generic presentable: we just display the -description. Not very exciting.

(defmethod display-object ((presentable-object presentable) (output-device stream))
  (format output-device "~A~&" (presentable-description presentable-object)))

(defmethod display-object-briefly ((presentable-object presentable) (output-device stream))
  (format output-device "~A~&" (presentable-description presentable-object)))

(defmethod request-object ((integer-valued-inputable-object integer-valued-inputable) (output-device stream) (input-device stream))
  (format output-device "Enter an integer> ")
  (finish-output output-device)
  (setf (current-state integer-valued-inputable-object) (parse-integer (read-line input-device) :junk-allowed t)))

(defmethod request-object ((one-line-string-valued-inputable-object one-line-string-valued-inputable) (output-device stream) (input-device stream))
  (format output-device "Enter a string> ")
  (finish-output output-device)
  (setf (current-state one-line-string-valued-inputable-object) (read-line input-device)))