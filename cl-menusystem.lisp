;;;; cl-menusystem.lisp

;;; I wasn't sure where to put the defpackage.

(defpackage #:cl-menusystem (:use #:common-lisp)
  (:export
   ;; presentable.lisp
   #:presentable
   #:presentable-description
   #:extended-help
   #:present-object
   #:display-object
   #:display-object-briefly
   #:request-object
   #:simple-format-message
   ;; menus.lisp
   #:menu
   #:presentables
   #:menu-tag
   ;; inputable.lisp
   #:inputable
   #:current-state
   #:instance-valid-input-predicate
   #:valid-inputable-state-p
   #:integer-valued-inputable
   #:one-line-string-valued-inputable
   #:expression-valued-inputable
   #:flags-valued-inputable
   #:available-flags
   #:flag-descriptions
   ;; easy-menus.lisp
   #:*menu-output*
   #:*menu-input*
   #:make-menu
   #:defmenu
   #:menu-action
   #:make-menu-action
   #:do-menu
   #:make-state-presentable
   #:make-state-presentable-no-hiding
   #:request-integer
   #:request-expression
   #:request-one-line-string
   #:make-list-menu
   #:make-preference-menu))
