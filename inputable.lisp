;;;; inputable.lisp

(in-package #:cl-menusystem)

(defclass inputable (requestable)
  ((current-state :initarg :current-state :accessor current-state)
   (instance-valid-input-predicate :initarg :instance-valid-input-predicate :accessor instance-valid-input-predicate :initform #'(lambda (e) t))))

(defmethod initialize-instance :after ((inputable-object inputable) &rest whatever)
  (declare (ignore whatever))
  (if (not (typep (instance-valid-input-predicate inputable-object) 'function))
      (error "instance-valid-input-predicate must be a function!")))

(defgeneric valid-inputable-state-p (inputable-object))

(defmethod valid-inputable-state-p ((inputable-object inputable))
  (funcall (instance-valid-input-predicate inputable-object) inputable-object))

(defclass integer-valued-inputable (inputable) ())

(defmethod valid-inputable-state-p :around ((inputable-object integer-valued-inputable))
  (if (not (slot-boundp inputable-object 'current-state))
      (error "current-state must be bound!")
    (if (not (typep (current-state inputable-object) 'integer))
        (values nil "This input must be an integer.")
       (if (next-method-p) (call-next-method) t))))

(defclass one-line-string-valued-inputable (inputable) ())

(defmethod valid-inputable-state-p :around ((inputable-object one-line-string-valued-inputable))
  (if (not (slot-boundp inputable-object 'current-state))
      (error "current-state must be bound!")
       (if (position #\Newline (current-state inputable-object))
           (values nil "This string must not contain a newline.")
         (if (next-method-p) (call-next-method) t))))

(defclass flags-valued-inputable (inputable)
  ((available-flags :initarg available-flags :accessor available-flags)
   (flag-descriptions :initarg flag-descriptions :accessor flag-descriptions :initform nil)))

(defmethod valid-inputable-state-p :around ((inputable-object flags-valued-inputable))
  (if (not (slot-boundp inputable-object 'current-state))
      (error "current-state must be bound!")
       (let ((the-state (current-state inputable-object))
             (flags (available-flags inputable-object)))
         (if (not (listp the-state))
             (error "current-state must be a list!")
           (let ((bad-val (some #'(lambda (e) (if (not (member e flags :test #'eql)) e nil)) the-state)))
             (if bad-val
                 (values nil (format nil "The flag ~A is not an acceptable flag!" bad-val))
               (if (next-method-p) (call-next-method) t)))))))

(defmethod initialize-instance :after ((fvi flags-valued-inputable) &rest whatever)
  (declare (ignore whatever))
  (if (not (slot-boundp fvi 'available-flags))
      (error "available-flags must be bound!")
    (if (not (and (consp 'available-flags) (listp 'available-flags)))
        (error "available-flags must be a non-empty list!")))
  (if (not (and (slot-boundp fvi 'flag-descriptions) (listp (flag-descriptions fvi))))
      (error "flag-descriptions must be an alist (possibly empty)")))