(defpackage #:cl-menusystem-test
  (:use #:cl-menusystem #:common-lisp)
  (:export *test-menu* *a-string*))

(in-package #:cl-menusystem-test)

(defvar *a-string* "Hi!")

(defmenu *test-menu* "A Menu" "Some Help"
  (make-menu-action "Hello world!" nil
		    (rst) (format t "Hello yourself!~&"))
  (make-menu "A random submenu" "Select an option from this submenu"
	     (make-menu-action "Jump!" nil
			       (rst) (format t "Whee!~&"))
	     (make-menu-action "Exit to previous menu" nil
			       (rst) (invoke-restart rst)))
  (make-menu-action "A submenu with two exits" nil
		    (toplevel-rst) (do-menu (make-menu "Two exits!" nil
						       (make-menu-action "Exit this submenu" nil
									 (rst) (invoke-restart rst))
						       (make-menu-action "Exit all the way" nil
									 (rst) (invoke-restart toplevel-rst "Foo!")))
					    *menu-output*
					    *menu-input*))
  #|(make-menu "A preference" "This submenu allows you to edit cl-menusystem-test:*a-string*"
	     (make-state-presentable "Value of *a-string*" (format nil "*a-string*'s value is currently: ~A" *a-string*))
	     (make-menu-action "Edit the value of *a-string*" nil
			       (rst) (setf *a-string* (request-one-line-string *menu-output* *menu-input* "Set the value of *a-string*")))
	     (make-menu-action "Exit to previous menu" nil
			       (rst) (invoke-restart rst)))|#
  (make-preference-menu cl-menusystem-test:*a-string*
			:description "a string"
			:extended-help "Please enter your name!"
			:type :one-line-string
			:present-form #'(lambda () (format nil "~A" *a-string*))
			:default-values (("a random value" "Hello, world!"))
			:convert-function #'(lambda (v) (format nil "Hello, ~A!" v)))
  (make-menu-action "Exit this menu" nil
		    (rst) (invoke-restart rst 42)))