;;;; tty-sys.lisp - cl-menusystem-sys definitions for the tty interface

(defpackage #:cl-menusystem-sys
  (:use #:common-lisp #:cl-menusystem #:cl-menusystem-tty)
  (:export #:sys-menu
	   #:do-menu-loop
	   #:do-inputable-loop))

(in-package #:cl-menusystem-sys)

(defclass sys-menu (tty-menu) ())

(defmacro do-menu-loop (menu-object output-device input-device)
  `(cl-menusystem-tty:do-menu-loop-tty ,menu-object ,output-device ,input-device))

(defmacro do-inputable-loop (inputable-object output-device input-device)
  `(cl-menusystem-tty:do-inputable-loop-tty ,inputable-object ,output-device ,input-device))