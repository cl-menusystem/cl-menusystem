;;;; presentable.lisp

(in-package #:cl-menusystem)

(defclass presentable ()
  ((presentable-description :initarg :presentable-description :accessor presentable-description)
   (extended-help :initarg :extended-help :accessor extended-help :initform nil)))

(defclass requestable (presentable) ())

(defmethod initialize-instance :after ((presentable-object presentable) &rest whatever)
  (declare (ignore whatever))
  (if (not (and (slot-boundp presentable-object 'presentable-description) (typep (presentable-description presentable-object) 'string)))
      (error "presentable-description must contain a valid string.")))

(defgeneric present-object (presentable-object output-device input-device))

(defgeneric display-object (presentable-object output-device))

(defgeneric display-object-briefly (presentable-object output-device))

(defgeneric request-object (requestable-object output-device input-device))

(defmethod present-object ((requestable-object requestable) output-device input-device)
  (display-object requestable-object output-device)
  (request-object requestable-object output-device input-device))

(defgeneric simple-format-message (output-device control-string &rest format-args))