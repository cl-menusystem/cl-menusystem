;;;; Silly emacs, this is -*- Lisp -*-

(defpackage #:cl-menusystem-system (:use #:asdf #:common-lisp))
(in-package #:cl-menusystem-system)

(defsystem cl-menusystem
  :author "Brian Mastenbrook <bmastenb@cs.indiana.edu>"
  :description "Easy and powerful menus for Common Lisp programs, with TTY backend"
  :components ((:file "cl-menusystem")
	       (:file "presentable" :depends-on ("cl-menusystem"))
	       (:file "menus" :depends-on ("presentable"))
	       (:file "inputable" :depends-on ("presentable"))
	       (:file "tty-interface" :depends-on ("menus"
						   "inputable"))
	       (:file "tty-sys" :depends-on ("tty-interface"))
	       (:file "easy-menus" :depends-on ("tty-sys" "menus" "inputable"))))

(defmethod perform :after ((o load-op) (c (eql (find-system :cl-menusystem))))
  (provide 'cl-menusystem))