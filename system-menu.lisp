;; system-menu.lisp, a Common Lisp system-definition manipulation front-end,
;; by Brian T Rice <water@tunes.org>. This is released freely for any use
;; under the MIT public license.
;;
;; This is the system-menu package, a user utility designed as a wrapper
;; for managing ASDF and mk-defsystem based systems in Common Lisp. It is
;; based on the package cl-menusystem.
;;
;; This first release is a core setup with enough features to be useful.
;;
;; Notes
;; -----
;; system-menu is currently SBCL-specific and only supports ASDF-based
;; systems. Portability and abstraction to cover mk-defsystem functionality
;; will come soon.
;;
;; For this release, system-menu comes with a custom version of cl-menusystem.
;; This is merely a convenience until some local enhancements made by
;; system-menu are incorporated.
;;
;; Installing
;; ----------
;; Installing is simply a matter of downloading this file into your local
;; lisp library directory (perhaps "~/lisp/") and loading it.
;;
;; Usage
;; -----
;; Call (sysmenu) and invoke the options by typing in numbers for the menu
;; entries. Explore it to see what the options are. There aren't many yet.

#+sbcl
(progn
  (require :asdf)
  (require :asdf-install)
  ;(require :mk-defsystem)
  (require :cl-menusystem)
)

(defpackage :system-menu
  (:export #:sysmenu)
  (:use :cl-menusystem :cl))

(in-package #:system-menu)

(defun sysdef-list-from-dir (dir &key (type "asd"))
  (cl:directory
   (merge-pathnames (make-pathname :name :wild :type type :version :wild)
		    dir)))

(defun sysdef-central-registry-list ()
  "Mirrors asdf's sysdef-central-registry-search."
  (loop for path-expr in asdf:*central-registry*
    for path = (eval path-expr)
    for pathname = (if (pathnamep path) path (pathname path))
    for dir = (ignore-errors (sysdef-list-from-dir pathname))
    when (consp dir)
    nconc dir))

(defvar *local-systems* '()
  "Locally-installed and known system definitions.")

(defvar *remote-systems* '()
  "Known non-local systems.")

(defun local-systems-refresh ()
  "Scans the appropriate areas for locally-installed system definitions."
  (loop for sysdef in (sysdef-central-registry-list)
    do (pushnew sysdef *local-systems* :key #'truename :test #'equal))
  (sort *local-systems* #'string<= :key #'namestring)
  *local-systems*)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (local-systems-refresh))

(defmethod display-object ((sys asdf:system) output-device)
  (handler-bind ((unbound-slot (lambda (x) nil)))
    (mapcar
     (lambda (pair)
       (simple-format-message output-device "~A: ~A~&" (car pair) (cdr pair)))
     `(("Name"       . ,(asdf:component-name sys))
       ("Version"    . ,(asdf:component-version sys))
       ("Description" . ,(asdf:system-description sys))
       ("Author"     . ,(asdf:system-author sys))
       ("Maintainer" . ,(asdf:system-maintainer sys))
       ("License"    . ,(asdf::system-licence sys))
       ("Status"     . nil))))) ;TODO: get package installation status

(defmenu *system-menu* "System Menu" nil
  (make-menu-action
   "List local systems" nil (rst)
   (if *local-systems*
       (progn (simple-format-message *menu-output* "Local Systems:~&")
	      (loop for each in *local-systems*
		for index from 0 by 1
		do (simple-format-message *menu-output* "~A: ~A~&"
					  index (pathname-name each))))
     (simple-format-message *menu-output* "No Local Systems Found.~&")))
  (make-menu-action
   "Auto-install a new system" "Uses asdf-install to obtain a system." (rst)
    (asdf-install:install
     (intern (request-one-line-string
	      *menu-output* *menu-input* "System Name:"
	      "Enter the name of an asdf-installable system; this should match its name on CLiki.net"))))
  (make-menu-action
   "Select a system to operate on" nil (main-rst)
   (let ((sys-pathname
	  (nth (request-integer
		*menu-output* *menu-input*
		"Which system do you want to work on?")
	       *local-systems*)))
     (do-menu
      (make-menu
       (format nil "Operating on system ~A" sys-pathname)
       "Select an action to perform."
       (make-menu-action
	"Show Extended Information" nil (rst)
	(display-object (asdf:find-system (pathname-name sys-pathname))
			*menu-output*))
       (make-menu-action "Compile" nil (rst)
			 (asdf:oos 'asdf:compile-op sys-pathname))
       (make-menu-action "Load" nil (rst)
			 (asdf:oos 'asdf:load-op sys-pathname))
       (make-menu-action "Load Source" nil (rst)
			 (asdf:oos 'asdf:load-source-op sys-pathname))
       (make-menu-action "Back to the main menu" nil (rst)
			 (invoke-restart rst)))
      *menu-output* *menu-input*)))
  (make-menu-action
   "Edit the ASDF central registry"
   "Edit the list of pathnames to look for ASDF definition files." (rst)
   (do-menu (make-list-menu asdf:*central-registry*
			    :description "The list of directory pathnames for ASDF to search for system definitions.")
	    *menu-output* *menu-input*))
  (make-menu-action "Exit" nil (rst) (invoke-restart rst)))

(defun sysmenu () (do-menu *system-menu* *menu-output* *menu-input*))

;; Creates an easy access point.
(in-package :cl-user)

(import '(system-menu:sysmenu))
(export '(system-menu:sysmenu))
