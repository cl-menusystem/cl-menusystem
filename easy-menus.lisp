;;;; easy-menus.lisp - macros and classes which make the construction of menus easier

(in-package #:cl-menusystem)

(defvar *menu-output* *query-io*)
(defvar *menu-input* *query-io*)

(defmacro with-gensyms (names &body forms)
  `(let ,(mapcar #'(lambda (name) (list name '(gensym))) names)
     ,@forms))

(defmacro generate-do-menu-action (menu-object output-device input-device)
  (with-gensyms
   (start-entry previous-menu end-entry menu-object-name chosen current-state return-value v)
   `(let ((,menu-object-name ,menu-object)
          (*menu-output* ,output-device)
          (*menu-input* ,input-device)
	  (,return-value nil))
      (tagbody
       ,start-entry
       (let ((,current-state (cl-menusystem-sys:do-menu-loop ,menu-object-name *menu-output* *menu-input*)))
	 (when (eql ,current-state (1+ (length (presentables ,menu-object-name)))) ; help was chosen
	   (if (not (extended-help ,menu-object-name))
	       (simple-format-message *menu-output* "Please choose a valid option from the menu. (No extended help is available.)~&")
	     (simple-format-message *menu-output* "~A~&" (extended-help ,menu-object-name)))
	   (go ,start-entry))
	 (let ((,chosen (nth (1- ,current-state) (presentables ,menu-object-name))))
	   (cond ((typep ,chosen 'menu-action)
		  (restart-case
		   (progn
		     (funcall (action ,chosen) ',previous-menu)
		     (go ,start-entry))
		   (,previous-menu (&optional ,v) (setf ,return-value ,v) (go ,end-entry))))
		 ((typep ,chosen 'menu)
		  (do-menu ,chosen *menu-output* *menu-input*)
		  (go ,start-entry))
		 ((typep ,chosen 'inputable)
		  (cl-menusystem-sys:do-inputable-loop ,chosen *menu-output* *menu-input*)
		  (go ,start-entry))
		 ((typep ,chosen 'presentable)
		  (display-object ,chosen *menu-output*)
		  (go ,start-entry)))))
      ,end-entry) ,return-value)))

(defclass easy-menu (cl-menusystem-sys:sys-menu)
  ((do-menu-action :initarg :do-menu-action :accessor do-menu-action)))

(defmacro make-menu (description help &rest menu-items)
  `(make-instance 'easy-menu :presentable-description ,description
		  :extended-help ,help
		  :presentables (list ,@menu-items)
		  :do-menu-action #'(lambda (menu-object object-device input-device)
				      (generate-do-menu-action menu-object object-device input-device))))

(defmacro defmenu (name description help &rest menu-items)
  `(defparameter ,name (make-menu ,description ,help ,@menu-items)))

(defclass menu-action (menu-tag)
  ((action :initarg :action :accessor action)))

(defmacro make-menu-action (description help lambda-args &body lambda-form)
  (if (not (eql (length lambda-args) 1))
      (error "lambda-args must be of length 1!"))
  `(make-instance 'menu-action :presentable-description ,description
		  :extended-help ,help
		  :action #'(lambda ,lambda-args ,@lambda-form)))

(defun do-menu (menu-object output-device input-device)
  (funcall (do-menu-action menu-object) menu-object output-device input-device))

(defclass state-presentable (presentable)
  ((display-state-function :accessor display-state-function :initarg :display-state-function)
   (hide-state-p :accessor hide-state-p :initarg :hide-state-p :initform nil)))

(defmethod display-object ((presentable-object state-presentable) output-device)
  (simple-format-message output-device "~A~&" (presentable-description presentable-object))
  (simple-format-message output-device "~A~& "(funcall (display-state-function presentable-object))))

(defmethod display-object-briefly ((presentable-object state-presentable) output-device)
  (if (hide-state-p presentable-object)
      (simple-format-message output-device "State hidden for: ~A~&" (presentable-description presentable-object))
    (simple-format-message output-device "~A: ~A~&" (presentable-description presentable-object) (funcall (display-state-function presentable-object)))))

(defmacro make-state-presentable (description &body forms)
  `(make-instance 'state-presentable
		  :presentable-description ,description
		  :extended-help nil
		  :hide-state-p t
		  :display-state-function (lambda () ,@forms)))

(defmacro make-state-presentable-no-hiding (description &body forms)
  `(make-instance 'state-presentable
		  :presentable-description ,description
		  :extended-help nil
		  :hide-state-p nil
		  :display-state-function (lambda () ,@forms)))

(defun request-one-line-string (output-device input-device description &optional extended-help &key valid-test)
  (let ((foo (if valid-test
		 (make-instance 'one-line-string-valued-inputable
				:presentable-description description
				:extended-help extended-help
				:instance-valid-input-predicate #'(lambda (i) (funcall valid-test (current-state i))))
	       (make-instance 'one-line-string-valued-inputable
			      :presentable-description description
			      :extended-help extended-help))))
    (cl-menusystem-sys:do-inputable-loop foo output-device input-device)))

(defun request-integer (output-device input-device description &optional extended-help &key valid-test)
  (let ((foo (if valid-test
		 (make-instance 'integer-valued-inputable
				:presentable-description description
				:extended-help extended-help
				:instance-valid-input-predicate #'(lambda (i) (funcall valid-test (current-state i))))
	       (make-instance 'integer-valued-inputable
			      :presentable-description description
			      :extended-help extended-help))))
    (cl-menusystem-sys:do-inputable-loop foo output-device input-device)))

(defun request-expression (output-device input-device description &optional extended-help &key valid-test)
  (let ((foo (make-instance 'one-line-string-valued-inputable
			    :presentable-description description
			    :extended-help (format nil "Please enter an expression on one line.~%~%~A" extended-help)
			    :instance-valid-input-predicate
			    #'(lambda (v)
				(let ((*read-eval* nil))
				  (handler-case
				   (let ((rfs (read-from-string (current-state v))))
				     (if valid-test (funcall valid-test rfs) t))
				   (reader-error (c) (let ((*print-escape* t)) (values nil (format nil "Reader error: ~A" c))))
				   (end-of-file () (values nil "Please enter an expression."))))))))
    (values (read-from-string (cl-menusystem-sys:do-inputable-loop foo output-device input-device)))))

(defmethod display-object ((seq list) output-device)
  (loop for each in seq
    for index from 0 by 1
    do (simple-format-message output-device "~A: ~A~&" index each)))

(defmethod display-object ((seq vector) output-device)
  (loop for each across seq
    for index from 0 by 1
    do (simple-format-message output-device "~A: ~A~&" index each)))

(defmacro make-list-menu (var &key description)
  `(make-menu
    ,description ,(format nil "Editing the list ~A" var)
    (make-menu-action
     ,(format nil "Value of ~A" var) nil (rst)
     (display-object ,var *menu-output*)
     ,var)
    (make-menu-action
     "Add an item" "Adds the item requested" (rst)
     (nconc ,var `((request-expression *menu-output* *menu-input*
				       "What object to add?"))))
    (make-menu-action
     "Modify an item" "Replaces a certain item with another one" (rst)
     (setf (nth (request-integer *menu-output* *menu-input*
				 "Which index do you want to modify?")
		,var)
	   (request-expression
	    *menu-output* *menu-input* "What value to replace it with?")))
    (make-menu-action
     "Delete an item" "Deletes the item requested" (rst)
     (delete (request-one-line-string
	      *menu-output* *menu-input* "Which value to delete?") ,var))
    (make-menu-action
     "Exit this menu" nil (rst)
     (invoke-restart rst ,var))))
			    
(defmacro make-preference-menu (var &key description extended-help type present-form default-values valid-test (convert-function #'(lambda (v) v)))
  `(make-menu ,description ,extended-help
	      (make-state-presentable ,(format nil "Current value of ~A" description) (funcall ,present-form))
	      (make-menu-action ,(format nil "Edit the value of ~A" description) nil
				(rst) (setf ,var (funcall ,convert-function
							  (,(cond ((eq type :one-line-string)
								   'request-one-line-string)
								  ((eq type :integer)
								   'request-integer)
								  ((eq type :expression)
								   'request-expression)
								  (t (error "Unknown type of preference: ~A" type)))
							   *menu-output*
							   *menu-input*
							   ,(format nil "Edit the value of ~A" description)
							   nil ,@(if valid-test `(:valid-test ,valid-test))))))
	      ,@(mapcar #'(lambda (v) `(make-menu-action ,(format nil "Set to ~A" (car v)) nil
							(rst) (setf ,var ,(cadr v)))) default-values)
	      (make-menu-action "Exit to previous menu" nil (rst) (invoke-restart rst))))
