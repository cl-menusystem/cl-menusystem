(defpackage #:asdf-interface
  (:use #:asdf #:cl-menusystem #:common-lisp)
  (:export asdf-menu))

(in-package #:asdf-interface)

(defmenu *test-menu* "ASDF Controller Menu" "Please select an option to control ASDF, Another System Definition Facility"
  (make-menu "ASDF settings" nil
	     (make-menu-action "Edit the value of *central-registry*" nil
			       (c-r-rst)
			       (do-menu (make-menu "Editing *central-registry*" nil
						   (make-menu-action "Show values in *central-registry*" nil
								     (rst)
								     (let ((n 0))
								       (mapcar #'(lambda (e)
										   (incf n)
										   (display-object-briefly
										    (make-state-presentable-no-hiding
										     (format nil "Item ~A" n) (format nil "~A" e)) *menu-output*)) *central-registry*)))
						   (make-menu-action "Done editing this value" nil
								     (rst) (invoke-restart rst))
						   (make-menu-action "Done editing values" nil
								     (rst) (invoke-restart c-r-rst))) *menu-output* *menu-input*))
	     (make-menu-action "Done editing settings" nil
			       (rst) (invoke-restart rst)))
  (make-menu-action "Exit this menu" nil
		    (rst) (invoke-restart rst)))

#|  (make-preference-menu cl-menusystem-test:*a-string*
			:description "a string"
			:extended-help "Please enter your name!"
			:type :one-line-string
			:present-form #'(lambda () (format nil "~A" *a-string*))
			:default-values (("a random value" "Hello, world!"))
			:convert-function #'(lambda (v) (format nil "Hello, ~A!" v)))|#