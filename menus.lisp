;;;; menus.lisp - CL "menu" implementation

(in-package #:cl-menusystem)

(defclass menu (requestable)
  ((presentables :initarg :presentables :accessor presentables :initform nil)))

(defclass menu-tag (presentable) ())