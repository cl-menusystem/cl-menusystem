cl-menusystem documentation as of 2003/09/10:

This is a prelimary attempt at documenting some of the functions that
would be more commonly used from cl-menusystem.

PACKAGE cl-menusystem:
Contains most of the functions that a user of cl-menusystem will need
to use.

PACKAGE cl-menusystem-sys:
Interfaces to the display system of the host.

MACRO make-menu (description help &rest menu-items:
Constructs a new menu with the given description and help, where
menu-items is a list of instances of menu, menu-action, inputable, or
presentable.

MACRO defmenu (name description help &rest menu-items)
Expands to (defvar name (make-menu description help menu-items))

MACRO make-menu-action (description help lambda-args &body lambda-form
Makes an instance of menu-action, which can be thought of as menu
entry that when invoked executes the body of lambda-form. The new
menu-action will have description and help as supplied here. The
lambda-args must be a list of precisely one symbol, which will be
bound to a restart during execution of lambda-form. Invoking this
restart will exit out of the menu that this menu-action is invoked
from. If a value is supplied to the restart it will be used as the
return value of the menu.

FUNCTION do-menu (menu-object output-device input-device)
Invoke and run the menu supplied using output-device and
input-device. During execution of the menu the special variables
*menu-output* and *menu-input* are bound to the supplied
output-device and input-device respectively. It is important to note
that the menu does not provide an "exit menu" option; you must
construct one using make-menu-action that invokes the supplied
restart. do-menu returns any value passed to that restart if supplied
or nil otherwise.

MACRO make-state-presentable (description &body forms)
This macro returns an instance of state-presentable which displays
the result of executing forms using the "~A" format control keyword.

FUNCTION request-one-line-string (output-device input-device
description &optional extended-help &key valid-test)
Requests a one line string from the given output and input devices,
possibly displaying description and extended-help. If valid-test is
supplied then this test will be applied to the input. valid-test must
be a lambda of one argument, a given value of the string. If the
return value of valid-test is nil it should return a second value
indicating why the input is not valid.

FUNCTION request-integer (output-device input-device description
&optional extended-help &key valid-test)
Requests an integer from the given output and input devices, possibly
displaying description and extended-help. If valid-test is supplied
then this test will be applied to the input (after checking to see if
the input is an integer is done.) valid-test must be a lambda of one
argument, a given integer value. If the return value of valid-test is
nil it should return a second value indicating why the input is not
valid.

MACRO make-preference-menu (var &key description extended-help type
present-form default-values valid-test convert-function)
Constructs a preference menu for the given variable (passed
unquoted). The description, type, present-form, and valid-test key
arguments must be supplied. The type argument must be a valid type for
a supplied request- function supplied as a key symbol (due to package
requirements) that will be used as the type for the input. (For
instance, input of a pathname should be done as a :one-line-string.)
present-form must be a function of zero arguments that returns a
string representing the current value of the
preference. default-values is a list of lists of two values - one a
string that will be used to name this value and the other a value for
the preference. valid-test is a lambda that will be called on the
input representation (eg string value) and will be used to determine
if the input is valid. (If this function returns nil it must return a
second value indicating why the input is invalid.) convert-function
must be a lambda of one argument that takes a valid input (for example
a string for a pathname) and returns it in the form it will be stored
in (eg converting a string into a pathname).
